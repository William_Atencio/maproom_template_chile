<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Examples_Precipitation" />
<title>Anomalie de Précipitations Precipitacion Mensuelle</title>
<link rel="stylesheet" type="text/css" href="../../../uicore/uicore.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Anomaly.html?Set-Language=en" />
<link class="share" rel="canonical" href="Anomaly.html" />
<meta xml:lang="" property="maproom:Entry_Id" content="Examples_Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCEP/.CPC/.CAMS_OPI/.v0208/.anomaly/.prcp/T/%28days%20since%201960-01-01%29streamgridunitconvert/T/differential_mul/T/%28months%20since%201960-01-01%29streamgridunitconvert//units/%28mm/month%29def/prcp_anomaly_max500_colors2//long_name/%28Precipitation%20Anomaly%29def/DATA/-500/-450/-400/-350/-300/-250/-200/-150/-100/-50/-25/25/50/100/150/200/250/300/350/400/450/500/VALUES/prcp_anomaly_max500_colors2+X+Y+fig-+colors+|+thin+grey+unlabelledcontours+black+thin+solid+coasts+countries+-fig+//aprod/-500/500/plotrange/T/last/plotvalue/X/-20/340/plotrange/Y/-65/75/plotrange+//plotborder+0+psdef//antialias+true+psdef//plotaxislength+220+psdef//XOVY+null+psdef//color_smoothing+null+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg share" name="bbox" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg dlauximg" name="plotaxislength" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Cuarto de mapas</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Examples/">Ejemplos</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"><span property="term:label">Ejemplos2</span></legend>
            </fieldset> 
            <fieldset class="navitem" id="chooseRegion"> 
                <legend>Regi&#243n</legend> 
            </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243n</a></li>
      <li><a href="#tabs-2" >Documentaci&#243n Dataset</a></li>
      <li><a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.CAMS_OPI/.v0208/.anomaly/.prcp/T/%28days%20since%201960-01-01%29streamgridunitconvert/T/differential_mul/T/%28months%20since%201960-01-01%29streamgridunitconvert//units/%28mm/month%29def/prcp_anomaly_max500_colors2//long_name/%28Precipitation%20Anomaly%29def/DATA/-500/-450/-400/-350/-300/-250/-200/-150/-100/-50/-25/25/50/100/150/200/250/300/350/400/450/500/VALUES/prcp_anomaly_max500_colors2/">Dataset</a></li>
      <li><a href="#tabs-3" >Cont&#225ctanos</a></li>
    </ul>
<fieldset class="dlimage" id="content" about="">
<link rel="iridl:hasFigure" href="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Peru/.40_pct_Deficit/daily_precip_colors/DATA/0/AUTO/RANGE//symmetric/false/def/X/Y/fig-/colors/grey/states/black/countries/blue/lakes/-fig/figviewer.html?map.here.x=212&map.here.y=303&map.url=+&map.domain=+%7B+Y+-18.270916+-0.070915+plotrange+%7D&map.domainparam=+%2Fplotaxislength+432+psdef+%2Fplotborder+72+psdef&map.X.width=12.60001&map.Y.width=18.2&map.plotaxislength=432&map.plotborder=72&map.fnt=Helvetica&map.fntsze=12&map.color_smoothing=1&map.XOVY=auto" />
<img class="dlimg" src="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Peru/.40_pct_Deficit/X/Y/fig-/colors/coasts/countries/states/lakes/-fig/++Y/-19/1/plotrange//plotborder+72+psdef//plotaxislength+432+psdef//XOVY+null+psdef/+//plotborder+72+psdef//plotaxislength+432+psdef//XOVY+null+psdef+.gif" border="0" alt="image" />
  <br />
<img class="dlauximg" src="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Peru/.40_pct_Deficit/daily_precip_colors/DATA/0/AUTO/RANGE//symmetric/false/def/X/Y/fig-/colors/grey/states/black/countries/blue/lakes/-fig/.auxfig+Y/-18.27092/-0.070915/plotrange+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Anomalias de Precipitaci&#243n Mensual</h2>
<p align="left" property="term:description">Las anomalias de précipitations sont basées sur des estimations dérivées de données satellitaires au-dessus des océans et sur des données de pluviomètres sur la terre ferme.  Les unités sont en milimètres (mm).  Les contours sont tracés à +/- 10, 25, 50,100,...,500 mm.  La période utilisée pour calculer la climatologie est 1979-2000 (les estimations satellitaires des précipitations ne remontent que jusqu'à 1979).</p>
<p align="left">"CAMS" est un acronyme pour "Climate Anomaly Monitoring System", système du Climate Prediction Center (CPC) au US National Centers for Environmental Prediction (NCEP).  "OPI" siginifie "Outgoing longwave radiation Precipitation Index" (les estimations satellitaires de précipitations sont basées sur des radiations de grande longueur d'onde émises par la Terre et observées par des satellites en orbite polaire). 
</p>
</div>
<div id="tabs-2" class="ui-tabs-panel">
<h2  align="center">Documentaci&#243n</h2>
<h4><a class="carry">Anomalias Precipitaci&#243n Mensual</a></h4>
<dl class="datasetdocumentation">
<dt>Données</dt><dd>CAMS_OPI v0208 anomalie de précipitations mensuelle sur une grille lat/lon 2.5° x 2.5° </dd>
<dt>Source</dt><dd>NOAA NCEP Climate Prediction Center, <a href="http://www.cpc.ncep.noaa.gov/products/global_precip/html/wpage.cams_opi.html">CAMS_OPI</a></dd>
<dt>Période de Référence pour la Climatologie</dt><dd>1979-2000</dd>
</dl>
</div>
<div class="ui-tabs-panel-hidden">
<h2 align="center">Dataset</h2>
<p>
<a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.CAMS_OPI/.v0208/.anomaly/.prcp/T/%28days%20since%201960-01-01%29streamgridunitconvert/T/differential_mul/T/%28months%20since%201960-01-01%29streamgridunitconvert//units/%28mm/month%29def/prcp_anomaly_max500_colors2//long_name/%28Precipitation%20Anomaly%29def/DATA/-500/-450/-400/-350/-300/-250/-200/-150/-100/-50/-25/25/50/100/150/200/250/300/350/400/450/500/VALUES/prcp_anomaly_max500_colors2/">Acceder a los dataset usado por este mapa.</a>
</p>
</div>
<div id="tabs-3"  class="ui-tabs-panel">
<h2  align="center">Contacto</h2>
<p>
Contact <a href="mailto:watencio@ana.gob.pe?subject=Anomalie de Précipitations Mensuelle">watencio@ana.gob.pe</a> with any technical questions or problems with this Map Room.
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
